/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.List;
import java.sql.SQLException;
import model.data.UserModel;
import model.pojo.User;


/**
 *
 * @author ASUS
 */
public class UserController {
    public java.util.List<User> loadUsers() throws SQLException{
       UserModel model = new UserModel();
       return model.loadUser();
    }
    
    public User login(String email, String password) throws SQLException{
        UserModel model = new UserModel();
        return model.login(email, password);
    }
}
