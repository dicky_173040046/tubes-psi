/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.pojo;

/**
 *
 * @author Adhy
 */
public class Assistant {
    private String id_assistant, name, email, password, role_id;
    
    public Assistant() {
        id_assistant = "ALIF00";
        name = null;
        email = null;
        password = "12345";
        role_id = "2";
    }
    
    public Assistant(String id_assistant, String name, String email, String password) {
        this.id_assistant = id_assistant;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getId_assistant() {
        return id_assistant;
    }

    public void setId_assistant(String id_assistant) {
        this.id_assistant = id_assistant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }
    
}
