/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.pojo;

/**
 *
 * @author Dicky Ardianto
 */
public class Admin {
    private String name, email, password, id_role;
    
    public Admin() {
        name = null;
        email = null;
        password = "12345";
        id_role = "1";
    }
    
    public Admin(String name, String email, String password, String id_role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.id_role = id_role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId_role() {
        return id_role;
    }

    public void setId_role(String id_role) {
        this.id_role = id_role;
    }
    
}
