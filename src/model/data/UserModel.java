/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.data;

import Utilities.DatabaseUtilities;
import java.awt.List;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.pojo.User;

/**
 *
 * @author ASUS
 */
public class UserModel {
    public java.util.List<User> loadUser() throws  SQLException{
        java.util.List<User> userList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT * FROM user");
            userList = new ArrayList<>();
            while (rs.next()) {                
                User u = new User();
                u.setName(rs.getString("name"));
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                u.setRole_id(rs.getInt("role_id"));
                
                userList.add(u);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return  userList;
    }
    
        public User login(String email, String password) throws SQLException{
        Connection con = DatabaseUtilities.getConnection();
        User user = new User();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT * FROM user WHERE email='"+email+"' AND password='"+ password+"'"); 
            if (rs.next()) {
                user.setRole_id(rs.getInt("role_id"));
                System.out.println("berhasil" +email + password + user.getRole_id());
                return user;
            }
            System.out.println("Gagal" +email + password + user.getRole_id());
            return null;
        } finally{
            if (con != null) {
                con.close();
            }
        }
    }
    }


