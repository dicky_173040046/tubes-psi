/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Utilities.DatabaseUtilities;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import view.AdminPageMenuAssistantFrame;
import view.LoginFrame;

/**
 *
 * @author Dicky Ardianto
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        if(con != null) {
            new LoginFrame().setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Koneksi Anda belum terhubung.");
        }
    }
}
